# AlpineLinux with python 2.7 and pyoidc configured for Login and ED attributes
#
# docker build -t middleware/pyoidc-op:v1.0.0-SNAPSHOT . && docker run --rm -it middleware/pyoidc-op:v1.0.0-SNAPSHOT

FROM dock1.db.vt.edu:1100/alpine:v3.3VT3
MAINTAINER Middleware <middleware@vt.edu>
LABEL name="pyoidc-docker" \
      description="AlpineLinux with python 2.7 and pyoidc configured for Login auth and claims from ED" \
      version="1.1.0-SNAPSHOT" \
      maintainer="middleware@vt.edu"

# GLIBC Environment
ENV GLIBC_CHECKSUM="9ee756072edafedb65bfe6835566c98ee58dee8ea073820df112c104b0de116e" \
    GLIBC_VERSION=2.21-r2

# Install cURL
# Pull glibc build from Andy Shinn 
# See here: https://github.com/gliderlabs/docker-alpine/issues/11
# Also see: https://developer.atlassian.com/blog/2015/08/minimal-java-docker-containers/
RUN apk --update add curl ca-certificates tar && \
    curl -Ls https://circle-artifacts.com/gh/andyshinn/alpine-pkg-glibc/6/artifacts/0/home/ubuntu/alpine-pkg-glibc/packages/x86_64/glibc-$GLIBC_VERSION.apk > /tmp/glibc.apk && \
    echo "$GLIBC_CHECKSUM  /tmp/glibc.apk" | sha256sum -c - && \
    apk add --allow-untrusted /tmp/glibc.apk && \
    rm /tmp/glibc.apk

# Install pyoidc and related dependencies
RUN apk --update add python py-setuptools gcc libc-dev python-dev libffi-dev openssl-dev py-gdbm && \
    apk --update add py-cherrypy py-pyldap --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ && \
    curl -Lo /tmp/pyoidc.tar.gz https://github.com/serac/pyoidc/archive/vt-fixes.tar.gz && \
    tar -zxvf /tmp/pyoidc.tar.gz -C /tmp && \
    cd /tmp/pyoidc-vt-fixes && \
    python setup.py install && \
    curl -Lo /usr/local/share/ca-certificates/ca-cert-VT-Root-CA.pem https://www.middleware.vt.edu/pubs/certs/vtrootca.pem && \
    update-ca-certificates

# Install sqlitedict library used as persistent dictionary implementation
# Stores OIDC client registrations
RUN curl -Lo /tmp/sqlitedict.tar.gz https://github.com/piskvorky/sqlitedict/archive/master.tar.gz && \
    tar -zxvf /tmp/sqlitedict.tar.gz -C /tmp && \
    cd /tmp/sqlitedict-master && \
    python setup.py install

# Apply pyoidc configuration for OP mode at Virginia Tech
ENV LOG_DIR=/apps/data/logs \
    TMPDIR=/tmp \
    LDAPTLS_CACERT=/usr/local/share/ca-certificates/ca-cert-VT-Root-CA.pem
#USER appsadm
RUN mkdir -p /apps/local/pyoidc-op
COPY app /apps/local/pyoidc-op/

ENTRYPOINT ["/apps/local/pyoidc-op/bin/server.sh", "start"]
