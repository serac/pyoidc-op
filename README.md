pyoidc-op: Python OpenID Connect Provider for Docker
====================================================

Capabilities in brief:
1. Login service for authentication
2. ED for claims

Prequisites
-----------
1. The ED-ID service used for ED integration must have a service password and have visibility on any attributes
listed in the config.py file specified at service startup time.
2. A DATA volume mounted from the host (can be a NAS share) with the following structure:

    ```
    $DATA
    ├── certs
    │   ├── server.crt
    │   └── server-ca-chain.pem
    ├── logs
    ├── private
    │   ├── server.key
    └── public
    ```

Building
--------
    docker build -t middleware/pyoidc-op:v1.0.0-SNAPSHOT .

Running
-------
1. Copy config.py.example to $DATA/private/config.py and edit for your environment.
2. Generate OIDC signing and encryption keys:
    openssl genrsa -out $DATA/private/rsa-enc.pem 2048
    openssl genrsa -out $DATA/private/rsa-sig.pem 2048
3. `docker run -v $DATA:/apps/data middleware/pyoidc-op:v1.0.0-SNAPSHOT -p 8443:8443`
