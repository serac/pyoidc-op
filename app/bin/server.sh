#!/bin/sh

PORT=8443
LOG_DIR=/apps/data/logs
PIDFILE=${TMPDIR%/}/oidc_op.pid
ROOT=/apps/local/pyoidc-op

if [ $# -lt 1 ]; then
  echo "USAGE: $(basename $0) start|stop|restart|status"
  exit
fi

case $1 in
start)
  python $ROOT/src/server.py -p $PORT -d /apps/data/private/config
  PID=$!
  echo -n $PID > $PIDFILE
  echo "Started OIDC OP server as process $PID"
  ;;
stop)
  if [ -f $PIDFILE ]; then
    PID=$(cat $PIDFILE)
    echo "Stopping OIDC OP server process $PID"
    kill $PID
    rm $PIDFILE
  else
    echo "OIDC OP server not running"
  fi
  ;;
restart)
  $0 stop
  $0 stop
  ;;
status)
  if [ -f $PIDFILE ]; then
    PID=$(cat $PIDFILE)
    RUNNING=$(kill -0 $PID;echo -n $?)
    if [ $RUNNING -eq 0 ]; then
      echo "OIDC OP server is running as process $PID"
      exit
    else
      echo "OIDC OP server is not running"
      echo "Removing stale PID file $PIDFILE"
      echo rm $PIDFILE
    fi
  else
    echo "OIDC OP server is not running"
  fi
  ;;
*)
  echo "Unsupported operation \"$1\""
  ;;
esac

