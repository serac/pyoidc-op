# -*- coding: utf-8 -*-

baseurl = "https://hostname.vt.edu"
issuer = "%s:%%d/" % baseurl
keys = [
    {"type": "RSA", "key": "/path/to/data/private/rsa-sign.pem", "use": ["sig"]},
    {"type": "RSA", "key": "/path/to/data/private/rsa-enc.pem", "use": ["enc"]},
    {"type": "EC", "crv": "P-256", "use": ["sig"]},
    {"type": "EC", "crv": "P-256", "use": ["enc"]}
]

CAS_SERVER = "https://login-dev.middleware.vt.edu/profile/cas"
SERVICE_URL = "%s/verify" % issuer

AUTHORIZATION = {
    "CAS": {"ACR": "CAS", "WEIGHT": 1, "URL": SERVICE_URL}
}

COOKIE_NAME = 'pyoidc'
COOKIE_TTL = 4*60
SYM_KEY = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
SERVER_CERT = "/path/to/server.crt"
SERVER_KEY = "/path/to/server.key"
CERT_CHAIN = "/path/to/server-ca-chain.pem"

# Path to data directory
# Must contain a "public" data where public, dynamic Web assets live
DATA_DIR = "/path/to/data"

# Path to private directory where sensitive data is stored
PRIVATE_DIR = "/path/to/data/private/"

LDAP = {
    "uri": "ldap://ed-dev.middleware.vt.edu",
    "tls": True,
    "base": "ou=people,dc=vt,dc=edu",
    "filter_pattern": "(uupid=%s)",
    "user": "uusid=your-ed-id-service,ou=services,dc=vt,dc=edu",
    "passwd": "XXXXXXXXXX",
    "attr": ["personType", "mail", "displayName", "sn", "givenName"],
    "attrmap": {
        "sub": "uupid",
        "name": "displayName",
        "given_name": "givenName",
        "family_name": "sn",
        "middle_name": "middleName",
        "nickname": "displayName",
        "preferred_username": "uupid",
        "profile": "labeledURI",
        "picture": "jpegPhoto",
        "website": "labeledURI",
        "email": "mailPreferredAddress",
        "email_verified": "mailPreferredAddress",
        "gender": "gender",
        "birthdate": "dateOfBirth",
        "phone_number": "telephoneNumber",
        "address": "postalAddress"
    }
}

USERINFO = "LDAP"
